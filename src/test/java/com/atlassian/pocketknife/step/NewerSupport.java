package com.atlassian.pocketknife.step;

import io.atlassian.fugue.Either;
import io.atlassian.fugue.Option;

public final class NewerSupport {

    static Either<AnError, String> firstOKEither(String str) {
        return Either.right(str);
    }

    static Either<AnError, String> firstNGEither(String str) {
        return Either.left(AnError.FIRST.ERROR);
    }

    static Either<AnError, Long> secondNGEither(Long number) {
        return Either.left(AnError.SECOND.ERROR);
    }

    static Either<AnError, Long> secondOKEither(Long number) {
        return Either.right(number);
    }

    static Either<AnError, Boolean> thirdOKEither(Boolean value) {
        return Either.right(!value);
    }

    static Either<AnError, Boolean> thirdNGEither(Boolean value) {
        return Either.left(AnError.THIRD.ERROR);
    }

    static Either<AnError, String> fourthOKEither(String value) {
        return Either.right(value.toLowerCase());
    }

    static Either<AnError, String> fourthNGEither(String value) {
        return Either.left(AnError.FORTH.ERROR);
    }

    static Either<AnError, Long> fifthOKEither(Long value) {
        return Either.right(value / 2);
    }

    static Either<AnError, Long> fifthNGEither(Long value) {
        return Either.left(AnError.FIFTH.ERROR);
    }

    static Option<String> firstOKOption(String str) {
        return Option.some(str);
    }

    static Option<String> firstNGOption(String str) {
        return Option.none();
    }

    static Option<String> secondOKOption(String str) {
        return Option.some(str);
    }

    static Option<String> secondNGOption(String str) {
        return Option.none();
    }

    static Option<String> thirdOKOption(String str) {
        return Option.some(str);
    }

    static Option<String> thirdNGOption(String str) {
        return Option.none();
    }

}
