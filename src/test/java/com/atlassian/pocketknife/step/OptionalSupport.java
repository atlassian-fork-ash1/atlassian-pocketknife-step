package com.atlassian.pocketknife.step;

import java.util.Optional;

public final class OptionalSupport {

    static Optional<String> firstOKOptional(String str) {
        return Optional.of(str);
    }

    static Optional<String> firstNGOptional(String str) {
        return Optional.empty();
    }

    static Optional<String> secondOKOptional(String str) {
        return Optional.of(str);
    }

    static Optional<String> secondNGOptional(String str) {
        return Optional.empty();
    }

    static Optional<String> thirdOKOptional(String str) {
        return Optional.of(str);
    }

    static Optional<String> thirdNGOptional(String str) {
        return Optional.empty();
    }

}
