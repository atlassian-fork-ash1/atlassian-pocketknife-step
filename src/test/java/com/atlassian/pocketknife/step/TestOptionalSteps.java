package com.atlassian.pocketknife.step;

import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

import static com.atlassian.pocketknife.step.OptionalSupport.firstNGOptional;
import static com.atlassian.pocketknife.step.OptionalSupport.firstOKOptional;
import static com.atlassian.pocketknife.step.OptionalSupport.secondNGOptional;
import static com.atlassian.pocketknife.step.OptionalSupport.secondOKOptional;
import static com.atlassian.pocketknife.step.OptionalSupport.thirdOKOptional;

public class TestOptionalSteps {

    private static final String STRING = "123456";
    private static final String STRING_UPPERED = "QWERTY";
    private static final String STRING_LOWERED = "qwerty";
    private static final Long LONG = 123456L;
    private static final Long LONGLONG = 123456123456L;

    @Test
    public void test_1_step_success() {
        Optional<Long> stepped = Steps
                .begin(firstOKOptional(STRING))
                .yield(value1 -> new Long(value1));

        Assert.assertTrue("Should be success", stepped.isPresent());
        Assert.assertTrue("Should be 123456", stepped.get().equals(LONG));
    }

    @Test
    public void test_2_step_success() {
        Optional<Long> stepped = Steps
                .begin(firstOKOptional(STRING))
                .then((firstValue) -> secondOKOptional(STRING))
                .yield((value1, value2) -> new Long(value1 + value2));

        Assert.assertTrue("Should be success", stepped.isPresent());
        Assert.assertTrue("Should be 123456", stepped.get().equals(LONGLONG));
    }

    @Test
    public void test_3_step_success() {
        Optional<Long> stepped = Steps
                .begin(firstOKOptional(STRING))
                .then(() -> secondOKOptional(STRING))
                .then(() -> firstOKOptional(STRING))
                .yield((value1, value2, value3) -> new Long(value1 + value2));

        Assert.assertTrue("Should be success", stepped.isPresent());
        Assert.assertTrue("Should be 123456", stepped.get().equals(LONGLONG));
    }

    @Test
    public void test_4_step_success() {
        Optional<Long> stepped = Steps
                .begin(firstOKOptional(STRING))
                .then(() -> secondOKOptional(STRING))
                .then((first, second) -> Optional.of(first + second))
                .then(() -> thirdOKOptional(STRING))
                .yield((value1, value2, value3, value4) -> new Long(value3));

        Assert.assertTrue("Should be success", stepped.isPresent());
        Assert.assertTrue("Should be 123456", stepped.get().equals(LONGLONG));
    }

    @Test
    public void test_5_step_success() {
        Optional<Long> stepped = Steps
                .begin(firstOKOptional(STRING))
                .then(() -> secondOKOptional(STRING))
                .then((first, second) -> Optional.of(first + second))
                .then(() -> thirdOKOptional(STRING))
                .then(() -> thirdOKOptional(STRING))
                .yield((value1, value2, value3, value4, value5) -> new Long(value3));

        Assert.assertTrue("Should be success", stepped.isPresent());
        Assert.assertTrue("Should be 123456", stepped.get().equals(LONGLONG));
    }

    @Test
    public void test_1_step_failure() {
        Optional<Long> stepped = Steps
                .begin(firstNGOptional(STRING))
                .yield(value1 -> new Long(value1));

        Assert.assertTrue("Should be failure", !stepped.isPresent());
    }

    @Test
    public void test_2_step_failure() {
        Optional<Long> stepped = Steps
                .begin(firstNGOptional(STRING))
                .then(() -> secondOKOptional(STRING))
                .yield((value1, value2) -> new Long(value1));

        Assert.assertTrue("Should be failure", !stepped.isPresent());
    }

    @Test
    public void test_3_step_failure() {
        Optional<Long> stepped = Steps
                .begin(firstOKOptional(STRING))
                .then(() -> secondNGOptional(STRING))
                .then(() -> secondOKOptional(STRING))
                .yield((value1, value2, value3) -> new Long(value1));

        Assert.assertTrue("Should be failure", !stepped.isPresent());
    }

    @Test
    public void test_4_step_failure() {
        Optional<Long> stepped = Steps
                .begin(firstOKOptional(STRING))
                .then(() -> secondOKOptional(STRING))
                .then((s, s2) -> secondOKOptional(STRING))
                .then(() -> secondNGOptional(STRING))
                .yield((value1, value2, value3, value4) -> new Long(value1));

        Assert.assertTrue("Should be failure", !stepped.isPresent());
    }

    @Test
    public void test_5_step_failure() {
        Optional<Long> stepped = Steps
                .begin(firstOKOptional(STRING))
                .then(s -> secondOKOptional(STRING))
                .then((s, s2) -> secondOKOptional(STRING))
                .then(() -> secondOKOptional(STRING))
                .then((value1, value2, value3, value4) -> secondNGOptional(STRING))
                .yield((value1, value2, value3, value4, value5) -> new Long(value1));

        Assert.assertTrue("Should be failure", !stepped.isPresent());
    }


}
