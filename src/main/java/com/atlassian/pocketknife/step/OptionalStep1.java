package com.atlassian.pocketknife.step;

import com.atlassian.pocketknife.step.functions.Function1;
import com.atlassian.pocketknife.step.ops.OptionalStep;

import java.util.Optional;
import java.util.function.Supplier;

public class OptionalStep1<A> extends OptionalStep
{

    private final Optional<A> option1;

    OptionalStep1(Optional<A> option1)
    {
        this.option1 = option1;
    }

    public <B> OptionalStep2<A, B> then(Function1<A, Optional<B>> functor)
    {
        Optional<B> option2 = option1.flatMap(functor::apply);
        return new OptionalStep2<>(option1, option2);
    }

    public <B> OptionalStep2<A, B> then(Supplier<Optional<B>> functor)
    {
        Optional<B> Optional = option1.flatMap(e1 -> functor.get());
        return new OptionalStep2<>(option1, Optional);
    }

    public <Z> Optional<Z> yield(Function1<A, Z> function1)
    {
        return option1.map(function1::apply);
    }

}
