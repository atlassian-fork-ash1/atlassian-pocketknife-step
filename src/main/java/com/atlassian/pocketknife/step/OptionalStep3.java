package com.atlassian.pocketknife.step;

import com.atlassian.pocketknife.step.functions.Function3;
import com.atlassian.pocketknife.step.ops.OptionalStep;

import java.util.Optional;
import java.util.function.Supplier;

public class OptionalStep3<A, B, C> extends OptionalStep {

    private final Optional<A> option1;
    private final Optional<B> option2;
    private final Optional<C> option3;

    OptionalStep3(Optional<A> option1, Optional<B> option2, Optional<C> option3) {
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
    }

    public <D> OptionalStep4<A, B, C, D> then(Function3<A, B, C, Optional<D>> functor) {
        Optional<D> option4 = option1.flatMap(
                e1 -> option2.flatMap(
                        e2 -> option3.flatMap(
                                e3 -> functor.apply(e1, e2, e3))));
        return new OptionalStep4<>(option1, option2, option3, option4);
    }

    public <D> OptionalStep4<A, B, C, D> then(Supplier<Optional<D>> functor) {
        Optional<D> Optional = option1.flatMap(
                e1 -> option2.flatMap(
                        e2 -> option3.flatMap(
                                e3 -> functor.get())));
        return new OptionalStep4<>(option1, option2, option3, Optional);
    }

    public <Z> Optional<Z> yield(Function3<A, B, C, Z> functor) {
        return option1.flatMap(e1 -> option2.flatMap(e2 -> option3.map(e3 -> functor.apply(e1, e2, e3))));
    }

}
