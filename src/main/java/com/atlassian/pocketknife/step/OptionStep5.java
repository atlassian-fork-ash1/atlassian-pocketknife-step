package com.atlassian.pocketknife.step;

import com.atlassian.pocketknife.step.functions.Function5;
import io.atlassian.fugue.Option;

public class OptionStep5<A, B, C, D, E> {
    private final Option<A> option1;
    private final Option<B> option2;
    private final Option<C> option3;
    private final Option<D> option4;
    private final Option<E> option5;

    OptionStep5(Option<A> option1, Option<B> option2, Option<C> option3, Option<D> option4, Option<E> option5) {
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
        this.option5 = option5;
    }

    public <Z> Option<Z> yield(Function5<A, B, C, D, E, Z> functor) {
        return option1.flatMap(
                e1 -> option2.flatMap(
                        e2 -> option3.flatMap(
                                e3 -> option4.flatMap(
                                        e4 -> option5.map(
                                                e5 -> functor.apply(e1, e2, e3, e4, e5))))));
    }
}
