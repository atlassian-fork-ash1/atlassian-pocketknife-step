package com.atlassian.pocketknife.step;

import com.atlassian.pocketknife.step.functions.Function4;
import com.atlassian.pocketknife.step.ops.OptionalStep;

import java.util.Optional;
import java.util.function.Supplier;

public class OptionalStep4<A, B, C, D> extends OptionalStep {

    private final Optional<A> option1;
    private final Optional<B> option2;
    private final Optional<C> option3;
    private final Optional<D> option4;

    OptionalStep4(Optional<A> option1, Optional<B> option2, Optional<C> option3, Optional<D> option4) {
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
    }

    public <E> OptionalStep5<A, B, C, D, E> then(Function4<A, B, C, D, Optional<E>> functor) {
        Optional<E> option5 = option1.flatMap(
                e1 -> option2.flatMap(
                        e2 -> option3.flatMap(
                                e3 -> option4.flatMap(
                                        e4 -> functor.apply(e1, e2, e3, e4)))));

        return new OptionalStep5<>(option1, option2, option3, option4, option5);
    }

    public <E> OptionalStep5<A, B, C, D, E> then(Supplier<Optional<E>> functor) {
        Optional<E> Optional = option1.flatMap(
                e1 -> option2.flatMap(
                        e2 -> option3.flatMap(
                                e3 -> option4.flatMap(
                                        e4 -> functor.get()))));
        return new OptionalStep5<>(option1, option2, option3, option4, Optional);
    }

    public <Z> Optional<Z> yield(Function4<A, B, C, D, Z> functor) {
        return option1.flatMap(
                e1 -> option2.flatMap(
                        e2 -> option3.flatMap(
                                e3 -> option4.map(
                                        e4 -> functor.apply(e1, e2, e3, e4)))));
    }

}
